<!DOCTYPE html>
<html>
	<head>
			<title>Drop Down Select</title>
			<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
			<script type="text/Javascript">
					$(document).ready(function(){
						$("#myID").change(function(){
							if($(this).val()== "Ravindra"){
									$("#Ravi").show();
									$("#Chethan").hide();
									$("#Yathi").hide();
									$("#Plan").hide();
									$("#PlanInfo").hide();
							} else if($(this).val()=="Yathindra"){
								$("#Yathi").show();
								$("#Ravi").hide();
								$("#Chethan").hide();
								$("#Plan").hide();
								$("#PlanInfo").hide();
							}else if($(this).val()=="Chethan"){
								$("#Chethan").show();
								$("#Yathi").hide();
								$("#Ravi").hide();
								$("#Plan").hide();
								$("#PlanInfo").hide();
							}else if($(this).val()=="Plan"){
								$("#Plan").show();
								$("#Ravi").hide();
								$("#Chethan").hide();
								$("#Yathi").hide();
								$("#PlanInfo").show();
								
							}else{
								$("#Ravi").hide();
								$("#Chethan").hide();
								$("#Yathi").hide();
								$("#Plan").hide();
								$("#PlanInfo").hide();
							}
							
						});
						
						
					
					});
			</script>
			<script type="text/Javascript">
				$(document).ready(function(){
					$("#Plan").change(function(){
								if($(this).val()=="Monthly Plan"){
									$("#mPlan").show();
									$("#3mPlan").hide();
									$("#6mPlan").hide();
									$("#1yPlan").hide();
								}else if($(this).val()=="3 Month"){
									$("#mPlan").hide();
									$("#3mPlan").show();
									$("#6mPlan").hide();
									$("#1yPlan").hide();
								}else if($(this).val()=="6 Month"){
									$("#mPlan").hide();
									$("#3mPlan").hide();
									$("#6mPlan").show();
									$("#1yPlan").hide();
								}else if($(this).val()=="1 Year"){
									$("#mPlan").hide();
									$("#3mPlan").hide();
									$("#6mPlan").hide();
									$("#1yPlan").show();
								}else{
									$("#mPlan").hide();
									$("#3mPlan").hide();
									$("#6mPlan").hide();
									$("#1yPlan").hide();
								}
						});
					
				
				});
			</script>
			<Style>
				input[type=number]{
					width:10%;
					padding: 12px 38px;
					box-sizing: border-box;
					background-color: #3CBC8D;
					color: white;
					border: none;
				}
			</style>
	</head>
	
	<body>
		<form>
				<select id="myID">
					<option>Select Name :</Option>
					<option>Ravindra</option>
					<option>Yathindra</option>
					<option>Chethan</option>
					<option>Plan</option>
				</select>
				<br>
				<br>
				<select id="Plan" style="display: none">
				<option>Select Plan :</Option>
					<option>Monthly Plan</option>
					<option>3 Month</option>
					<option>6 Month</option>
					<option>1 Year</option>
				</select>
				<br/>
				<div id="dv">
				<p id="Ravi" style="display: none">Sonata Software Limited</p>
				<p id="Yathi" style="display: none">Cap Gemini India</p>
				<p id="Chethan" style="display: none">Dubai Company</p>
				</div>
				<br/>
				<!--
				<p id="mPlan" style="display: none">800</p>
				<P id="3mPlan" style="display: none">2100</p>
				<P id="6mPlan" style="display: none">4000</p>
				<P id="1yPlan" style="display: none">7500</p>-->
				<div id="PlanInfo">
				<input type = "number" id="mPlan" style="display: none" value="800" readonly>
				<input type = "number" id="3mPlan" style="display: none" value="2100" readonly>
				<input type = "number" id="6mPlan" style="display: none" value="4000" readonly>
				<input type = "number" id="1yPlan" style="display: none" value="7500" readonly>
				</div>
		</form>
	</body>
</html>