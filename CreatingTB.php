<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "InfoDB";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

// sql to create table
$sql = "CREATE TABLE MemberInfo (
AdmissionNumber INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
Name VARCHAR(100) NOT NULL,
Age INT,
AdmissionDate TIMESTAMP NOT NULL,
Address Varchar(255),
Category Varchar(100),
Plan VARCHAR(100),
email VARCHAR(100),
ReceiptNumber INT, 
PaidAmount INT NOT NULL,
Balance INT,
LastPaidDate varchar(50),
DueDate Varchar(50),
FaceBookName Varchar(100),
ADMIN Varchar(30),
CommentInfo Varchar(255)
)";

if ($conn->query($sql) === TRUE) {
    echo "Table MemberInfo created successfully";
} else {
    echo "Error creating table: " . $conn->error;
}

$conn->close();
?>