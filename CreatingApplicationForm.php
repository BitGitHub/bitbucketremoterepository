<!DOCTYPE html>
<html>
	<head>
		<title>REGISTRATION FORM</title>
		<script src="angular.min.js"></script>
		<script src="jquery-3.1.1.min.js"></script>
	</head>
		<script type="text/JavaScript">
			//Angular JS Library
			var app = angular.module("myApp",[]);
			app.controller("myCtrl",function($scope){
					
			});
		</script>
		<script type="text/JavaScript">
			//JQuery Library
			$(document).ready(function(){
				$("#myCat").change(function(){
					if($(this).val()=="STUDENT PLAN"){
						$("#myPlanS").show();
						$("#myPlanN").hide();
						$("#myFees1").hide();
						$("#myFees2").hide();
						$("#myFees3").hide();
						$("#myFees4").hide();
						$("#myFees5").hide();
						$("#myFees6").hide();
						$("#myFees7").hide();
						$("#myFees8").hide();
						$("#myPlanT").hide();
						$("#myFees9").hide();
						$("#myFees10").hide();
						$("#myPlan11").hide();
					}else if($(this).val()=="NORMAL PLAN"){
						$("#myPlanS").hide();
						$("#myPlanN").show();
						$("#myFees1").hide();
						$("#myFees2").hide();
						$("#myFees3").hide();
						$("#myFees4").hide();
						$("#myFees5").hide();
						$("#myFees6").hide();
						$("#myFees7").hide();
						$("#myFees8").hide();
						$("#myPlanT").hide();
						$("#myFees9").hide();
						$("#myFees10").hide();
						$("#myPlan11").hide();
					}else if($(this).val()=="PERSONEL TRAINER"){
						$("#myPlanS").hide();
						$("#myPlanN").hide();
						$("#myFees1").hide();
						$("#myFees2").hide();
						$("#myFees3").hide();
						$("#myFees4").hide();
						$("#myFees5").hide();
						$("#myFees6").hide();
						$("#myFees7").hide();
						$("#myFees8").hide();
						$("#myPlanT").show();
						$("#myFees9").hide();
						$("#myFees10").hide();
						$("#myPlan11").hide();
					}else{
						$("#myPlanS").hide();
						$("#myPlanN").hide();
						$("#myFees1").hide();
						$("#myFees2").hide();
						$("#myFees3").hide();
						$("#myFees4").hide();
						$("#myFees5").hide();
						$("#myFees6").hide();
						$("#myFees7").hide();
						$("#myFees8").hide();
						$("#myPlanT").hide();
						$("#myFees9").hide();
						$("#myFees10").hide();
						$("#myPlan11").hide();
					}
					
				});
				
			});
		</script>
		<script type="text/JavaScript">
				$(document).ready(function(){
					$("#myPlanSs").change(function(){
							if($(this).val()=="MONTHLY"){
								$("#myFees1").show();
								$("#myFees2").hide();
								$("#myFees3").hide();
								$("#myFees4").hide();
								$("#myFees5").hide();
								$("#myFees6").hide();
								$("#myFees7").hide();
								$("#myFees8").hide();
								$("#myFees9").hide();
								$("#myFees10").hide();
								$("#myFees11").hide();
							}else if($(this).val()=="3 MONTH"){
								$("#myFees1").hide();
								$("#myFees2").show();
								$("#myFees3").hide();
								$("#myFees4").hide();
								$("#myFees5").hide();
								$("#myFees6").hide();
								$("#myFees7").hide();
								$("#myFees8").hide();
								$("#myFees9").hide();
								$("#myFees10").hide();
								$("#myFees11").hide();
							}else if($(this).val()=="6 MONTH"){
								$("#myFees1").hide();
								$("#myFees2").hide();
								$("#myFees3").show();
								$("#myFees4").hide();
								$("#myFees5").hide();
								$("#myFees6").hide();
								$("#myFees7").hide();
								$("#myFees8").hide();
								$("#myFees9").hide();
								$("#myFees10").hide();
								$("#myFees11").hide();
							}else if($(this).val()=="1 YEAR"){
								$("#myFees1").hide();
								$("#myFees2").hide();
								$("#myFees3").hide();
								$("#myFees4").show();
								$("#myFees5").hide();
								$("#myFees6").hide();
								$("#myFees7").hide();
								$("#myFees8").hide();
								$("#myFees9").hide();
								$("#myFees10").hide();
								$("#myFees11").hide();
							}else{
								$("#myFees1").hide();
								$("#myFees2").hide();
								$("#myFees3").hide();
								$("#myFees4").hide();
								$("#myFees5").hide();
								$("#myFees6").hide();
								$("#myFees7").hide();
								$("#myFees8").hide();
								$("#myFees9").hide();
								$("#myFees10").hide();
								$("#myFees11").hide();
							}
					});
				});
		</script>
		
		<script type="text/JavaScript">
				$(document).ready(function(){
					$("#myPlanNs").change(function(){
							if($(this).val()=="MONTHLY"){
								$("#myFees1").hide();
								$("#myFees2").hide();
								$("#myFees3").hide();
								$("#myFees4").hide();
								$("#myFees5").show();
								$("#myFees6").hide();
								$("#myFees7").hide();
								$("#myFees8").hide();
								$("#myFees9").hide();
								$("#myFees10").hide();
								$("#myFees11").hide();
							}else if($(this).val()=="3 MONTH"){
								$("#myFees1").hide();
								$("#myFees2").hide();
								$("#myFees3").hide();
								$("#myFees4").hide();
								$("#myFees5").hide();
								$("#myFees6").show();
								$("#myFees7").hide();
								$("#myFees8").hide();
								$("#myFees9").hide();
								$("#myFees10").hide();
								$("#myFees11").hide();
							}else if($(this).val()=="6 MONTH"){
								$("#myFees1").hide();
								$("#myFees2").hide();
								$("#myFees3").hide();
								$("#myFees4").hide();
								$("#myFees5").hide();
								$("#myFees6").hide();
								$("#myFees7").show();
								$("#myFees8").hide();
								$("#myFees9").hide();
								$("#myFees10").hide();
								$("#myFees11").hide();
							}else if($(this).val()=="1 YEAR"){
								$("#myFees1").hide();
								$("#myFees2").hide();
								$("#myFees3").hide();
								$("#myFees4").hide();
								$("#myFees5").hide();
								$("#myFees6").hide();
								$("#myFees7").hide();
								$("#myFees8").show();
								$("#myFees9").hide();
								$("#myFees10").hide();
								$("#myFees11").hide();
							}else{
								$("#myFees1").hide();
								$("#myFees2").hide();
								$("#myFees3").hide();
								$("#myFees4").hide();
								$("#myFees5").hide();
								$("#myFees6").hide();
								$("#myFees7").hide();
								$("#myFees8").hide();
								$("#myFees9").hide();
								$("#myFees10").hide();
								$("#myFees11").hide();
							}
					});
				});
		</script>
		<script type="text/JavaScript">
				$(document).ready(function(){
					$("#myPlanTs").change(function(){
							if($(this).val()=="3 MONTH"){
								$("#myFees1").hide();
								$("#myFees2").hide();
								$("#myFees3").hide();
								$("#myFees4").hide();
								$("#myFees5").hide();
								$("#myFees6").hide();
								$("#myFees7").hide();
								$("#myFees8").hide();
								$("#myFees9").show();
								$("#myFees10").hide();
								$("#myFees11").hide();
							}else if($(this).val()=="6 MONTH"){
								$("#myFees1").hide();
								$("#myFees2").hide();
								$("#myFees3").hide();
								$("#myFees4").hide();
								$("#myFees5").hide();
								$("#myFees6").hide();
								$("#myFees7").hide();
								$("#myFees8").hide();
								$("#myFees9").hide();
								$("#myFees10").show();
								$("#myFees11").hide();
							}else if($(this).val()=="1 YEAR"){
								$("#myFees1").hide();
								$("#myFees2").hide();
								$("#myFees3").hide();
								$("#myFees4").hide();
								$("#myFees5").hide();
								$("#myFees6").hide();
								$("#myFees7").hide();
								$("#myFees8").hide();
								$("#myFees9").hide();
								$("#myFees10").hide();
								$("#myFees11").show();
							}else{
								$("#myFees1").hide();
								$("#myFees2").hide();
								$("#myFees3").hide();
								$("#myFees4").hide();
								$("#myFees5").hide();
								$("#myFees6").hide();
								$("#myFees7").hide();
								$("#myFees8").hide();
								$("#myFees9").hide();
								$("#myFees10").hide();
								$("#myFees11").hide();
							}
					});
				});
		</script>
		<script type="text/JavaScript">
			//$(document).ready(function(){
				//$('#myAgree').click(function() {
				//	$("#myUnChecked").toggle(this.checked);
				//});
				
			//});
		</script>
		<style>
			caption{
				background-color:orange;
				opacity:0.7;
				color:BLUE;
            }
		
			input[type=text]{
				text-transform:uppercase;
				width:100%;
			}
			input[type=number]{
				width:100%;
			}
			input[type=date]{
				width:100%;
			}
			td{
				color:white;
				font-weight=bold;
				font-family: "Arial Black", Gadget, sans-serif;
			}
		</style>
	<body background="GYM4.jpg">
			<form id="myFrom" action="SaveForm.php"ng-app="myApp" ng-controller="myCtrl" method="post" align="center" name="myForm">
					<table align="center" width="27%">
					<caption><h2>REGISTRATION FORM</h2></caption>
							<tr><td align='left'>NAME :</td><td align='left'><input type="text" name="myName" id = "myName" onkeyup="this.value=this.value.replace(/[^a-z]/g,'');"></td></tr>
							<tr><td align='left'>AGE :</td><td align='left'><input type="number" name="myAge" id="myAge"></td></tr>
							<tr><td align='left'>ADMISSION DATE :</td><td align='left'><input type="date" name="myDate" id="myDate"></td></td></tr>
							<tr><td align="left">PHONE NUMBER :</td><td align='left'><input type="number" name="myNumber" id="myNumber"></td></tr>
							<tr><td align="left">FACEBOOK ID :</td><td align='left'><input type="text" name="myFB" id="myFB" ></td></tr>
							<tr id = "myCt"><td align="left">CATEGORY :</td><td align='left'><select id="myCat">
							<option>SELECT CATEGORY</option>
							<option>STUDENT PLAN</option>
							<option>NORMAL PLAN</option>
							<option>PERSONEL TRAINER</option>
							</select></td></tr>
							<tr id="myPlanS" style="display: none"><td align="left">PLAN :</td><td align='left'><select id="myPlanSs">
								<option>SELECT PLAN</option>
								<option>MONTHLY</option>
								<option>3 MONTH</option>
								<option>6 MONTH</option>
								<option>1 YEAR</option>
							</select></td></tr>
							<tr id="myPlanN" style="display: none"><td align="left">PLAN :</td><td align='left'><select id="myPlanNs">
								<option>SELECT PLAN</option>
								<option>MONTHLY</option>
								<option>3 MONTH</option>
								<option>6 MONTH</option>
								<option>1 YEAR</option>
							</select></td></tr>
							<tr id="myPlanT" style="display: none"><td align="left">PLAN :</td><td align='left'><select id="myPlanTs">
								<option>SELECT PLAN</option>
								<option>3 MONTH</option>
								<option>6 MONTH</option>
								<option>1 YEAR</option>
							</select></td></tr>
							<tr id="myFees1" style="display: none"><td align="left" >FEES :</td><td ><input type="number" id="myFees1s"value="700" readonly></td></tr>
							<tr id="myFees2" style="display: none"><td align="left">FEES :</td><td><input type="number" id="myFees2s"value="1800" readonly></td></tr>
							<tr id="myFees3" style="display: none"><td align="left">FEES :</td><td><input type="number" id="myFees3s"value="3500" readonly></td></tr>
							<tr id="myFees4" style="display: none"><td align="left">FEES :</td><td ><input type="number" id="myFees4s"value="7000" readonly></td></tr>
							<tr id="myFees5" style="display: none"><td align="left">FEES :</td><td ><input type="number" id="myFees5s"value="800" readonly></td></tr>
							<tr id="myFees6" style="display: none"><td align="left">FEES :</td><td ><input type="number" id="myFees6s"value="2100" readonly></td></tr>
							<tr id="myFees7" style="display: none"><td align="left">FEES :</td><td><input type="number" id="myFees7s"value="4000" readonly></td></tr>
							<tr id="myFees8" style="display: none"><td align="left">FEES :</td><td ><input type="number" id="myFees7s"value="7800" readonly></td></tr>
							<tr id="myFees9" style="display: none"><td align="left">FEES :</td><td ><input type="number" id="myFees6s"value="7000" readonly></td></tr>
							<tr id="myFees10" style="display: none"><td align="left">FEES :</td><td ><input type="number" id="myFees7s"value="12000" readonly></td></tr>
							<tr id="myFees11" style="display: none"><td align="left">FEES :</td><td ><input type="number" id="myFees7s"value="20000" readonly></td></tr>
							<tr><td align="left">EMAIL :</td><td align="left"><input type="email" id="myEmail" id="myEmail"></td></tr>
							<tr><td align="left">PAID AMOUNT :</td><td align="left"><input type="number" id="myPaid" name="myPaid"></td></tr>
							<tr><td align="left">PAID DATE :</td><td align="left"><input type="Date" id="myPaidD" name="myPaidD"></td></tr>
							<tr><td align="left">RECEIPT NUMBER :</td><td align="left"><input type="number" id="myReceipt" name="myReceipt"></td></tr>
							<tr id="myChecked"><td align="left" rowspan="1">AGREE :</td><td align="left">AGREE :<input type="checkbox" name="myAgree" id="myAgree" checked></td></tr>
							<tr><td align="left">REMARKS :</td><td align="left"><textarea ></textarea></td></tr>
							<!--<tr id="myUnChecked"><td align="left">NOT AGREE :<input type="checkbox" id="myNotAgree" name="myNotAgree"></td></tr>-->
							
					</table>
					<input type="submit">
			</form>
	</body>
</html>